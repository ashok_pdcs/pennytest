import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {  SetRegion } from '../store/app-action';
import { AppStateModel, DATA_STATE, NgrxObject } from '../store/app-reducer';
// import { setSearchTextRegion } from '../store/store-selectors';

@Component({
  selector: 'app-supplier-store',
  templateUrl: './supplier-store.component.html',
  styleUrls: ['./supplier-store.component.scss']
})
export class SupplierStoreComponent implements OnInit {

  searchText: any;
  constructor(private store: Store<AppStateModel>) { }

  ngOnInit(): void {
  }

  filterRegion(text): void {
    this.store.dispatch(new SetRegion({ data: text }));
  }

}
