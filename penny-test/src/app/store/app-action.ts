import { Action } from '@ngrx/store';

export const APP_ACTIONS = {
    SET_SEARCH_TEXT_REGION: 'SET_SEARCH_TEXT_REGION',
    SET_SEARCH_TEXT_NAME: 'SET_SEARCH_TEXT_NAME'
};

export class SetRegion implements Action {
    readonly type = APP_ACTIONS.SET_SEARCH_TEXT_REGION;
    constructor(public payload ) {}
}
export class SetName implements Action {
    readonly type = APP_ACTIONS.SET_SEARCH_TEXT_NAME;
    constructor(public payload ) {}
}

export type AppActions = SetRegion | SetName;
