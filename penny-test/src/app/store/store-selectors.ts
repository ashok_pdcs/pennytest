
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AppStateModel } from './app-reducer';

const getAppState = createFeatureSelector<AppStateModel>('app');
export const setSearchTextName = createSelector(getAppState, (state) => {
    return state.searchTextName;
});


export const setSearchTextRegion = createSelector(getAppState, (state) => {
    return state.searchTextRegion;
});


export const combineLatest = createSelector(setSearchTextName, setSearchTextRegion, (name , region) => {
    return {
        name,
        region
    };
});
