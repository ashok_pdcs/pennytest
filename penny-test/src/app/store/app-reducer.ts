
import { APP_ACTIONS } from './app-action';

export const DATA_STATE = {
    INITIAL: 'Initial',
    RESOLVED: 'Resolved',
    EMPTY: 'Empty'
};

export interface NgrxObject {
    data: string;
    state: string;
}

export interface AppStateModel {
    searchTextRegion: NgrxObject;
    searchTextName: NgrxObject;

}

export const defaultFactsState: AppStateModel = {
    searchTextRegion: {
        data: '',
        state: DATA_STATE.INITIAL
    },
    searchTextName: {
        data: '',
        state: DATA_STATE.INITIAL
    },


};

export function appReducer(state = defaultFactsState, action): NgrxObject | AppStateModel {
    switch (action.type) {
        case APP_ACTIONS.SET_SEARCH_TEXT_NAME:
            return {
                ...state, searchTextName: {
                    data: action.payload.data.trim().toLowerCase(),
                    state: stateMapper(action.payload.data)
                }
            };
        case APP_ACTIONS.SET_SEARCH_TEXT_REGION:
            return {
                ...state, searchTextRegion: {
                    data: action.payload.data.trim().toLowerCase(),
                    state: stateMapper(action.payload.data)
                }
            };

        default:
            return state;

    }

}

export function stateMapper(data): any {
    return data.trim().length ? DATA_STATE.RESOLVED : DATA_STATE.EMPTY;
}
