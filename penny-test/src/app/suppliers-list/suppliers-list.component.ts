import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppStateModel, DATA_STATE, NgrxObject } from '../store/app-reducer';
import { combineLatest, setSearchTextRegion } from '../store/store-selectors';
import { SUPPLIERS } from './suppliers-mock';
@Component({
  selector: 'app-suppliers-list',
  templateUrl: './suppliers-list.component.html',
  styleUrls: ['./suppliers-list.component.scss']
})
export class SuppliersListComponent implements OnInit {

  suppliers = [...SUPPLIERS];
  visibleSuppliers = [...SUPPLIERS];
  @ViewChildren('suppliersEl') suppliersElem: QueryList<any>;
  searchText: NgrxObject;
  paginationIndexes: { startIndex: number, endIndex: number };
  isGridView = true;
  constructor(private store: Store<AppStateModel>) { }

  ngOnInit(): void {
    this.store.select(combineLatest).subscribe((succ) => {
      if ([succ.name.state, succ.region.state].every((el) => el === DATA_STATE.RESOLVED)) {
        this.suppliers = this.suppliers.filter((el) => el.region.toLowerCase()
          .match(succ.region.data)).filter((el) => el.name.toLowerCase().match(succ.name.data));

      } else if ([succ.name.state, succ.region.state].some((el) => el === DATA_STATE.RESOLVED)) {
        const searchProp = succ.name.state === DATA_STATE.RESOLVED ? 'name' : 'region';
        // this.searchText = succ;
        this.suppliers = [...SUPPLIERS].filter((el) => el[searchProp].toLowerCase().match(succ[searchProp].data));
        this.setVisibleData(this.paginationIndexes, this.suppliers);
      } else if ([succ.name.state, succ.region.state].some((el) => el === DATA_STATE.EMPTY)) {
        this.suppliers = [...SUPPLIERS];
        this.setVisibleData(this.paginationIndexes, this.suppliers);
      }
    });

  }

  pageChange(data): void {
    this.paginationIndexes = data;
    this.setVisibleData(this.paginationIndexes, this.suppliers);
  }

  setVisibleData(indexes, data): void {
    this.visibleSuppliers = [...data].slice(indexes.startIndex, indexes.endIndex);
  }

  scrollToTop(): void {
    this.suppliersElem.first.nativeElement.scrollIntoView({ behavior: 'smooth' });
  }

  displayItems(isGridView): void {
    this.isGridView = isGridView;
  }

}
