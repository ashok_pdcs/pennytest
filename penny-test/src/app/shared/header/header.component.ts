import { Component, OnInit, Output , EventEmitter} from '@angular/core';
// import { EventEmitter } from 'stream';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Output() filterSuppliers = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  onSearch(searchText): void {
    this.filterSuppliers.emit(searchText);
  }
}
