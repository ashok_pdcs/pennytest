import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() btnText = 'Visit store';
  @Input() cardHeaderText = '';
  @Input() cardBodyText = '';
  @Input() cardFooterText = '';
@Input() iconUrl = '';
  constructor() { }

  ngOnInit(): void {
  }

}
