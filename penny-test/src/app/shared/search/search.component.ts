import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { fromEvent } from 'rxjs';
import {
  debounceTime,
  map,
  distinctUntilChanged,
} from 'rxjs/operators';
// import { EventEmitter } from 'stream';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Input() placeHolder = '';
  @ViewChild('searchBox', { static: true }) searchInput: ElementRef;
  @Output() searchText = new EventEmitter();
  emittedText = '';
  isSearchChanged = false;
  constructor() { }

  ngOnInit(): void {

    fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
      map((event: any) => {
        return event.target.value;
      })
      , debounceTime(1000)
      , distinctUntilChanged()
    ).subscribe((text: string) => {
      this.isSearchChanged = true;
    });

  }

  // tslint:disable-next-line:typedef
  apply() {
    const searchTxt = this.searchInput.nativeElement.value.trim();
    if (this.emittedText.trim() !== searchTxt) {
      this.searchText.emit(searchTxt);
      this.emittedText = searchTxt;
    }

  }
  onKeyUp(event): void {
    if (event.keyCode === 13) {
      this.apply();
    }
  }

}
