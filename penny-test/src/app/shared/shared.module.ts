import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CardComponent } from './card/card.component';
import { SearchComponent } from './search/search.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';



@NgModule({
  declarations: [HeaderComponent, FooterComponent, CardComponent, SearchComponent, PaginatorComponent],
  imports: [
    CommonModule, FormsModule
  ],
   exports: [HeaderComponent, FooterComponent, CardComponent, SearchComponent, PaginatorComponent]
})
export class SharedModule { }
