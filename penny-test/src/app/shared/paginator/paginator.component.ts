import { Component, OnInit, Output, EventEmitter, Input, SimpleChange, OnChanges, SimpleChanges } from '@angular/core';


@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit, OnChanges {

  readonly PAZE_SIZES = [10, 20, 30];
  selectedPageSize = 10;
  @Input() itemCount = 10;
  selectedPageNumber = 1;
  paginator = {
    firstPage: 1,
    prevPage: 1,
    nextPage: 1,
    lastPage: 1,

  };
  pageNumbers: Array<any> = [];
  @Output() pageChange = new EventEmitter();
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.itemCount.currentValue) {
      this.setPagainatorControls();
    }
  }
  ngOnInit(): void {
  }

  get isLast(): boolean {
    return Number(this.selectedPageNumber) === (this.pageNumbers.length);
  }

  get isFirst(): boolean {
    return Number(this.selectedPageNumber) === 1;
  }
  selectPageNumber(pageNumber): void {
    if (pageNumber < 1 || pageNumber > this.pageNumbers.length) {
      return;
    }
    this.selectedPageNumber = pageNumber;
    this.setPagainatorControls();
  }
  setPagainatorControls(end = 10): void {
    this.setPageNumbers();
    const endIndex = Math.min(this.selectedPageSize * this.selectedPageNumber, this.itemCount);
    this.pageChange.emit({
      startIndex: endIndex - this.selectedPageSize,
      endIndex
    });



  }
  setPageNumbers(): void {
    const totalPages = Math.ceil(this.itemCount / this.selectedPageSize);
    this.selectedPageNumber = Math.min(totalPages, this.selectedPageNumber);
    this.pageNumbers = [];
    for (let i = 1; i <= totalPages; i++) {
      this.pageNumbers.push({
        value: i,
        isSelected: i === this.selectedPageNumber,
        isVisibled: i - 2 < this.selectedPageNumber && this.selectedPageNumber < i + 2
      });
    }
  }


}
