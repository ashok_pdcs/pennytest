import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { CATEGORIES_LIST } from './categories-constants';
import { SetName } from './store/app-action';
import { AppStateModel } from './store/app-reducer';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'penny-test';
  categoriesLlist = CATEGORIES_LIST;
constructor(private store: Store<AppStateModel>){

}

  filterSuppliers(searchText): void{
      this.store.dispatch(new SetName({ data: searchText }));
  }
}
